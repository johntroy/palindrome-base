package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		//fail( "Test not created." );
		boolean isPalindrome = Palindrome.isPalindrome("racecar");
		assertTrue("Unable to validate palindrome", isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Troy");
		//fail( "Test not created." );
		assertFalse("Unable to validate palindrome", isPalindrome);
	}
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race Car");
		//fail( "Test not created." );
		assertTrue("Unable to validate palindrome", isPalindrome);
	}
	
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race a car");
		assertFalse("Unable to validate palindrome", isPalindrome);
		//fail( "Test not created." );
	}	
	
}
